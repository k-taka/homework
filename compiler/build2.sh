#!/bin/bash

output="calc"
yaccfile="task2.y"
lexfile="task2.l"


bison -dv -y "$yaccfile"
flex -l "$lexfile"
gcc y.tab.c lex.yy.c -ly -lfl -lm -o "$output"
