/* メモリ付き関数電卓 */

%{
#include <math.h>
#include <stdio.h>
int yylex(void);
#define M_SIZE 16

struct Mem {
  enum {I, D} Type;
  union {
    int I;
    double D;
  } V_fld;
} Memory[M_SIZE];

#define read_mem(E, M) \
  if (M.Type==I) { \
    E.Type=INT; \
    E.V_fld.I = M.V_fld.I; \
  } else { \
    E.Type=D; \
    E.V_fld.D = M.V_fld.D; \
  } \

#define write_mem(M, X) \
  if (X.Type==I) { \
    M.Type=I; \
    M.V_fld.I = X.V_fld.I; \
  } else { \
    M.Type=D; \
    M.V_fld.D = X.V_fld.D; \
  } \

#define TYPE_OP(E, X, OP, Y) \
  if (X.Type == INT && Y.Type == INT) { \
    E.Type = INT; \
    E.V_fld.I = X.V_fld.I OP Y.V_fld.I; \
  } else {\
    E.Type = DBL; \
    E.V_fld.D = (X.Type==INT ? (double)X.V_fld.I : X.V_fld.D) \
    OP (Y.Type==INT ? (double)Y.V_fld.I : Y.V_fld.D); \
  }

#define ASIGN(E, X) \
  if (X.Type == INT) { \
    E.Type = INT; \
    E.V_fld.I = X.V_fld.I; \
  } else { \
    E.Type = DBL; \
    E.V_fld.D = X.V_fld.D; \
  }

#define MINUS(E, X) \
  if (X.Type == INT) { \
    E.Type = INT; \
    E.V_fld.I = - X.V_fld.I; \
  } else { \
    E.Type = DBL; \
    E.V_fld.D = - X.V_fld.D; \
  } \

#define FN_DBL(E, FN, X) \
  E.Type = DBL; \
  if (X.Type == INT) { \
    E.V_fld.D = FN((double)X.V_fld.I); \
  } else { \
    E.V_fld.D = FN(X.V_fld.D); \
  } \

#define FN_DBL2(E, FN, X, Y) \
  double x = X.Type==INT ? (double)X.V_fld.I : X.V_fld.D; \
  double y = Y.Type==INT ? (double)Y.V_fld.I : Y.V_fld.D; \
  E.Type = DBL; \
  E.V_fld.D = FN(x, y); \
%}


%union{
  int    ival;
  double rval;
  struct {
    enum {INT, DBL} Type;
    union {
      int    I;
      double D;
    } V_fld;
  } Val;
}

%token        MEM EXP LOG SQRT COS FABS TINT TDBL
%token <ival> INTC
%token <rval> REALC

%type <ival> mcell
%type <Val> expr

%right '='
%left  '+' '-'
%left  '*' '/'
%right '^'
%right UMINUS

%%
line  :                                         /* 空規則 */
      | line expr  '\n'
      {
        if ($2.Type==INT) {
          printf("int: %d\n", $2.V_fld.I);
        } else {
          printf("double: %f\n", $2.V_fld.D);
        }
      }
      | line error '\n' { yyerrok; }
      ;

expr  : mcell '=' expr
      {
        ASIGN($$, $3);
        write_mem(Memory[$1], $3);
      }
      | expr '+' expr         { TYPE_OP($$, $1, +, $3); }
      | expr '-' expr         { TYPE_OP($$, $1, -, $3); }
      | expr '*' expr         { TYPE_OP($$, $1, *, $3); }
      | expr '/' expr         { TYPE_OP($$, $1, /, $3); }
      | expr '^' expr         { FN_DBL2($$, pow, $1, $3) }
      | '-' expr %prec UMINUS { MINUS($$, $2); }
      | '(' expr ')'          { ASIGN($$, $2); }
      | LOG  '(' expr ')'     { FN_DBL($$, log , $3); }
      | COS  '(' expr ')'     { FN_DBL($$, cos , $3); }
      | FABS '(' expr ')'     { FN_DBL($$, fabs, $3); }
      | EXP  '(' expr ')'     { FN_DBL($$, exp , $3); }
      | SQRT '(' expr ')'     { FN_DBL($$, sqrt, $3); }
      | '(' TINT ')' expr     { $$.Type=INT; $$.V_fld.I = $4.Type==INT ? $4.V_fld.I : (int)($4.V_fld.D); }
      | '(' TDBL ')' expr     { $$.Type=DBL; $$.V_fld.D = $4.Type==DBL ? $4.V_fld.D : (double)($4.V_fld.I); }
      | mcell                 { read_mem($$, Memory[$1]); }
      | INTC                  { $$.Type = INT; $$.V_fld.I = $1; }
      | REALC                 { $$.Type = DBL; $$.V_fld.D = $1; };
      ;

mcell : MEM '[' INTC ']' { if ($3 >= 0 && $3 < M_SIZE) {
                             $$ = $3;
                           } else {
			     printf("Memory Fault\n");
			     $$ = 0;
			   }
                         }
      ;

%%

int main()
{
  yyparse();                                   /* 式の構文解析と計算 */
}

int yyerror(char *msg)                             /* エラーメッセージの表示 */
{
  printf("%s\n", msg);
}
