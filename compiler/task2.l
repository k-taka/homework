/* メモリ付き関数電卓プログラムの字句定義 */

%{
#include "y.tab.h"
%}

ws     [ \t]
symbol [=+\-*/\^()[\]\n]
digit  [0-9]
other  .

%%

{ws}+
{symbol}            { return (yytext[0]); }
"M"                 { return (MEM); }
"exp"               { return (EXP); }
"log"               { return (LOG); }
"sqrt"              { return (SQRT); }
"cos"               { return (COS); }
"fabs"              { return (FABS); }
"INT"               { return (TINT); }
"DBL"               { return (TDBL); }
{digit}+            { sscanf(yytext, "%d", &yylval.ival);
                      return (INTC);
                    }
{digit}+"."{digit}* { sscanf(yytext, "%lf", &yylval.rval);
                      return (REALC);
                    }
{other}             { fprintf(stderr, "Illegal character '%c' ignored\n", yytext[0]);}

%%