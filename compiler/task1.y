/* 簡単な電卓 */
/*
* bison -y task1.y
* gcc y.tab.c -ly -o calc
* 
* TODO: 
* done % 演算子を使えるように
* done 空白・tab・改行以外の文字がでたらエラーメッセージを出力
* done 空白・tab・改行以外の文字がでたらその文字を無視
* done factorとして負の数値も扱えるように
* done ゼロ除算のときにエラーメッセージ
*/


%start line

%{
#include<stdio.h>
#include <stdlib.h>

int yylex();
int yyerror();
%}

%%
line   : expr '\n' { printf("res %d\n", $1); }
       ;

expr   : expr '+' term { $$ = $1 + $3; }
       | expr '-' term { $$ = $1 - $3; }
       | term { $$ = $1; }
       ;

term   : term '*' factor { $$ = $1 * $3; }
       | term '/' factor { if($3 == 0) {fprintf(stderr, "zero division exception\n"); exit(-1);} $$ = $1 / $3; }
       | term '%' factor { $$ = $1 % $3; }
       | factor { $$ = $1; }
       ;

factor : '(' expr ')' { $$ = $2; }
       | num { $$ = $1; }
       | '-' num { $$ = -$2; }
       ;

num    : '0' { $$ = 0; }
       | '1' { $$ = 1; }
       | '2' { $$ = 2; }
       | '3' { $$ = 3; }
       | '4' { $$ = 4; }
       | '5' { $$ = 5; }
       | '6' { $$ = 6; }
       | '7' { $$ = 7; }
       | '8' { $$ = 8; }
       | '9' { $$ = 9; }
       ;
%%
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int yylex()
{
  int c;
  char *allowed_c = "()+-*/0123456789\n";
  char *space_c = " \t";
  
  while (c = getchar(), !strchr(allowed_c, c)) {
      // 入力文字がallowed_cになければ実行される
      if (strchr(space_c, c)) continue;
      else {
        printf("invalid character: %c\n", c);
        continue;
      }
  }
  return c;
}
